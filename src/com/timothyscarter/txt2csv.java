package com.timothyscarter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import java.io.*;

import javax.swing.JFileChooser;

public class txt2csv {
	
	static String[] file_name = new String[10];
	static ArrayList<String> newText0 = new ArrayList<String>();
	static ArrayList<String> newText1 = new ArrayList<String>();
	static ArrayList<String> newText2 = new ArrayList<String>();
	static ArrayList<String> newText3 = new ArrayList<String>();
	static ArrayList<String> newText4 = new ArrayList<String>();
	static ArrayList<String> newText5 = new ArrayList<String>();
	static ArrayList<String> newText6 = new ArrayList<String>();
	static ArrayList<String> newText7 = new ArrayList<String>();
	static ArrayList<String> newText8 = new ArrayList<String>();
	static ArrayList<String> newText9 = new ArrayList<String>();
	
	public static void main (String[] args) {
		final JFileChooser fc = new JFileChooser();

		int NumFiles = 0;
		int file;
		
		fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		
		while(true) {
			file = fc.showOpenDialog(fc);
			// 0 = 'open' button; 1 = 'cancel' button
			File file1 = fc.getSelectedFile( );
			file_name[NumFiles] = file1.toString( );
			NumFiles++;
			if (file == JFileChooser.CANCEL_OPTION)
				break;
		}
		FileReader[] LineReader = new FileReader[10];
		BufferedReader[] LineBuffer = new BufferedReader[10];
		String line1 = null;
		try {

			for (int temp2 = 0; temp2<NumFiles; temp2++) {
				LineReader[temp2] = new FileReader(file_name[temp2]);
				LineBuffer[temp2] = new BufferedReader(LineReader[temp2]);
				try {
					while ((line1 = LineBuffer[temp2].readLine()) != null) {
						switch(temp2)
						{
						case 0:
							newText0.add(line1);
							break;
						case 1:
							newText1.add(line1);
							break;
						case 2:
							newText2.add(line1);
							break;
						case 3:
							newText3.add(line1);
							break;
						case 4:
							newText4.add(line1);
							break;
						case 5:
							newText5.add(line1);
							break;
						case 6:
							newText6.add(line1);
							break;
						case 7:
							newText7.add(line1);
							break;
						case 8:
							newText8.add(line1);
							break;
						case 9:
							newText9.add(line1);
							break;
						}
					}
					LineBuffer[temp2].close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			Writer OutputFile = new BufferedWriter (new OutputStreamWriter(new FileOutputStream("comics.csv")));
			for (int temp1=0; temp1<newText0.size(); temp1++) {
				OutputFile.append(newText0.get(temp1) + ",");
				if (NumFiles > 1)
					OutputFile.append(newText1.get(temp1) + ",");
				if (NumFiles > 2)
					OutputFile.append(newText2.get(temp1) + ",");
				if (NumFiles > 3)
					OutputFile.append(newText3.get(temp1) + ",");
				if (NumFiles > 4)
					OutputFile.append(newText4.get(temp1) + ",");
				if (NumFiles > 5)
					OutputFile.append(newText5.get(temp1) + ",");
				if (NumFiles > 6)
					OutputFile.append(newText6.get(temp1) + ",");
				if (NumFiles > 7)
					OutputFile.append(newText7.get(temp1) + ",");
				if (NumFiles > 8)
					OutputFile.append(newText8.get(temp1) + ",");
				if (NumFiles > 9)
					OutputFile.append(newText9.get(temp1) + ",");
				OutputFile.append("\r");
			}
			OutputFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}