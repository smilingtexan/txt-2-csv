# TXT 2 CSV
## by: Timothy Carter (aka: SmilingTexan)

This is a project I worked on some time ago. I needed a way to convert .txt files
to .csv files.

It was a very specific need, so modification would probably be necessary or any other needs.

Currently, the project will allow up to 10 input files. It will read a line from each file,
and assign that as one field in the .csv file (separated by commas, hence .csv).
